# scrabble

### Problem Statement :
Given a word "hat" of n letters, what are all the n-or-fewer-letter words in the word list that can be made from that "hat" word.

### Dictionary for scrabble :
The word list is available from the dictionary text file.

### Logic :
The program uses a Trie to store the words from the dictionary.

I wrote a web service for this scrabble game which when given a word would enlist all the words that can be formed from the letters of the given word and the sorted list of words is returned as the output of this web service. The words are sorted based on their score.

### Usage :
Request URL: http://hostname:8080/words/hat
Expected response: ["hat","ah","ha","th","at","a"]

### Output :
JSON list of strings is sorted by the word's scrabble value in highest to lowest scoring.

### Build, Test and Run the service

Project is using Maven as build tool and Java 8 version.

Maven can be installed from https://maven.apache.org/download.cgi

#### build and run test cases

```
cd scrabble
mvn clean install
```

#### Run service

```
java -jar target/scrabble-0.1.0.jar
```

Open Browser
```
http://localhost:8080/words/hat
```

* Time complexity of inserting `m` dictionary words in trie is `O(m x length_of_string)`.
* Time complexity of searching a word in trie is `O(length_of_string)`.
* Space complexity of storing `m` words in Trie is `O(ALPHABET_SIZE x length_of_string x m)`.
