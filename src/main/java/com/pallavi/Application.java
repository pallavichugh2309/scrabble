package com.pallavi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import com.pallavi.app.Trie;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.*;

/**
 *
 * Main configuration class for creating Spring application .
 * @author Pallavi
 *
 */

@SpringBootApplication
public class Application {

    @Autowired
    private Environment env; //Injecting properties file

    @Autowired
    private ResourceLoader resourceLoader;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    /**
     * @return Trie Bean.
     */
    @Bean
    public Trie getTrie() {
        Trie dictionary = new Trie();
        try {
            final Resource fileResource = resourceLoader.getResource(env.getProperty("com.pallavi.dictionary"));
            BufferedReader br = new BufferedReader(new InputStreamReader(fileResource.getInputStream()));
            String line = null;
            while ((line = br.readLine()) != null) {
                dictionary.insertWord(line);
            }
        } catch (IOException e) {
            System.out.println("File not found.");
        }
        return dictionary;
    }
}
