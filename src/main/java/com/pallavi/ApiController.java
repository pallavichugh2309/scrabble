package com.pallavi;

import com.pallavi.app.Trie;
import com.pallavi.app.Word;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Main controller class.
 *
 * @author Pallavi
 *
 */

@RestController //
public class ApiController {

    @Autowired
    Trie trie; //Injecting dictionary bean

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * @param name is a String using whose characters words are retrieved from the Trie
     * @return list of unique words found
     */
    @RequestMapping(value = {"/words", "/words/{name}"})
    public List<String> getWords(@PathVariable Optional<String> name) {
        if(name.isPresent()) {
            List<Character> chars = new ArrayList<>();

            for (char ch: name.get().toCharArray()) {
                chars.add(ch);
            }
            List<Word> output = new ArrayList<Word>();
            output.addAll(trie.searchWords(chars));
            Collections.sort(output);
            List<String> wordList = output.stream().map(Word::getWord).collect(Collectors.toList());
            return wordList;
        }
        else return new ArrayList<String>();

    }
}
