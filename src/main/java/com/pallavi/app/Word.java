package com.pallavi.app;

import java.util.List;
import java.util.ArrayList;

/**
 * the Word class stores a word found in the dictionary with its score calculated by the summission of individual character scores forming the word
 *
 * @author Pallavi
 *
 */
public class Word implements Comparable<Word>{

    private List<Character> alphabets;
    private int score;

    /**
     * default constructor initialising character list for a word and score property as 0
     */
    public Word(){
        alphabets = new ArrayList<Character>();
        score = 0;
    }

    /**
     * Constructor for appending a new character to a word object and update its score value
     * @param wordformedsoFar the existing word object to which a new character gets appended
     * @param newAlphabet the new character to be appended to the existing word object
     */
    public Word(Word wordformedsoFar, char newAlphabet) {
        alphabets = new ArrayList<Character>();
        for(char alphabet: wordformedsoFar.getAlphabets()){
            alphabets.add(alphabet);
        }
        alphabets.add(newAlphabet);
        score = wordformedsoFar.getScore() + getCharacterScore(newAlphabet);
    }

    /**
     * function returning the score for a given character
     * @param alphabet the character whose score to return
     * @return the score of the specified character
     */
    private int getCharacterScore(char alphabet) {
        // TODO Auto-generated method stub
        switch(alphabet){
            case 'a':
            case 'e':
            case 'i':
            case 'l':
            case 'n':
            case 'o':
            case 'u':
            case 'r':
            case 't':
            case 's':
                return 1;
            case 'd':
            case 'g':
                return 2;
            case 'b':
            case 'c':
            case 'm':
            case 'p':
                return 3;
            case 'f':
            case 'h':
            case 'v':
            case 'w':
            case 'y':
                return 4;
            case 'k':
                return 5;
            case 'x':
            case 'j':
                return 8;
            case 'q':
            case 'z':
                return 10;
        }
        return 0;
    }


    /**
     * function to return the String representation of the Word object
     * @return
     */
    public String getWord(){
        StringBuffer sb = new StringBuffer();
        for(char alphabet: alphabets){
            sb.append(alphabet);
        }
        return sb.toString();
    }

    /**
     * function to return the list of the alphabets in the Word object
     * @return
     */
    private List<Character> getAlphabets(){
        return alphabets;
    }


    /**
     * function to return the cumulative score of the Word object
     * @return
     */
    public int getScore() {
        // TODO Auto-generated method stub
        return score;
    }

    /**
     * function to check if the two word objects represent the same word
     * @return
     */
    @Override
    public boolean equals(Object obj){
        return getWord().compareTo(((Word)obj).getWord()) == 0;
    }

    /**
     * function to get the hashCode value of the word represented by the Word object
     * @return
     */
    @Override
    public int hashCode() {
        return getWord().hashCode();
    }

    /**
     * function to sort the words in the decreasing order of their scores
     */
    @Override
    public int compareTo(Word word) {
        return -Integer.compare(getScore(), word.getScore());
    }

    /**
     * function to return the length of the word represented by the Word object
     * @return
     */

    public int getWordLength(){
        return alphabets.size();
    }



}
