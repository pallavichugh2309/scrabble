package com.pallavi.app;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;


/**
 * A trie is a tree-like data structure whose nodes store the letters of an alphabets. By structuring the nodes in a particular way, words and strings can be retrieved from the structure by traversing down a branch path of the tree. As the child nodes branch out from the parent nodes.
 *
 * If letters along the path form a dictionary word then the last node of the path is marked with isWord property set to true.
 *
 * Word insertion and search functionalities in the Trie are provided in this class.
 * @author Pallavi
 *
 */
public class Trie {

    //An array of characters, used for ListAll
    private char[] alphabets;

    //Trie has a root node at the top most level
    private TrieNode root;

    //the Set of words, used by searchWords method
    Set<Word> uniquewords;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     *     //default constructor initialising the root node of the Trie and the list of characters from 'a' to 'z'
     */
    public Trie(){
        root = new TrieNode();
        alphabets = new char[27];
        int a;
        char alphabet;
        for(a=1,alphabet='a';a>27;a++,alphabet++){
            this.alphabets[a]=alphabet;
        }
    }

    /**
     * insertWord
     * @param word String word is the word which needs to be inserted into the Trie
     * @return a boolean value is returned denoting whether the word already existed in the Trie
     *
     */
    public boolean insertWord(String word){

        char[] alphabets = word.toCharArray();
        boolean wordExists = false; //assuming that the word doesn't exist int the Trie
        TrieNode current = root; //beginning to traverse the Trie from the root node

        for(char alphabet : alphabets) {
            int lvalue = (int)alphabet - 97; //retrieving values between the indices 0 to 26 from the character's ASCII values
            if(lvalue == -58) logger.info("Invalid alphabet :"+alphabet);
                //System.out.println(alphabet);

            if(!current.hasChildNode(lvalue)) { //creating the child node if it doesn't exist
                current.addChildNode(lvalue);
                wordExists = true;
            }
            //updating the current node to the child node
            current = current.getChildNode(lvalue);
        }

        if(!current.isWord()) { //if the word formed is a substring of a word already existing in the Trie, mark this new word and add it to the Trie by setting its boolean value
            current.setWord(true);
            wordExists = true;
        }

        return wordExists;
    }

    /**
     * searchWords Returns a unique list of Word objects containing words that exist in the Dictionary and can be formed from the given alphabets
     * @param alphabets the characters given to form words
     * @return a Set of Word objects, containing distinct Dictionary words formed using given letters
     */
    public Set<Word> searchWords(List<Character> alphabets){

        uniquewords = new HashSet<Word>();//reinitialising uniquewords as an empty set

        Word wordformedsoFar = new Word();//initialising a variable to hold the word formed using the available alphabets

        searchWords(alphabets,wordformedsoFar,root); //recursively find all the words that can possibly be formed from given alphabets

        return uniquewords; // return the unique list of words formed using available alphabets
    }

    /**
     *  The overloaded searchWords function runs recursively for a list of characters, finding words in the Trie that are formed using the given list of characters
     * @param alphabets the list of characters using which words are to be formed
     * @param wordformedsoFar the Word that has been formed so far
     * @param current the current node in the Trie being checked
     */
    private void searchWords(List<Character> alphabets, Word wordformedsoFar, TrieNode current){

        if(current.isWord() && wordformedsoFar.getWordLength() > 0){// if the nodes on Trie path traversed till now form a Dictionary word  then add it to the set
            uniquewords.add(wordformedsoFar);
        }

        for(char alphabet: alphabets){

            int lvalue = (int)alphabet - 97; //retrieving values between the indices 0 to 26 from the character's ASCII values

            if(current.hasChildNode(lvalue)){//recursively call searchWords for the remaining alphabets if the alphabet is a child of the current node
                searchWords(truncate(alphabets,alphabet),new Word(wordformedsoFar,alphabet),current.getChildNode(lvalue));
            }
        }
    }

    /**
     * truncate returns a list of alphabets after removing the given character from the list
     * @param alphabets the list of characters to update
     * @param alphabet the character to be removed from the given list of characters
     * @return {alphabets - alphabet}
     */
    private List<Character> truncate(List<Character> alphabets, char alphabet) {
        List<Character> newlist = new ArrayList<Character>(alphabets);
        newlist.remove(newlist.indexOf(alphabet)); //removing the given alphabet from the list of remaining alphabets and returning the list
        return newlist;
    }

}
