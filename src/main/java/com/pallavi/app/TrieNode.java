package com.pallavi.app;

/**
 * the TrieNode class creates, returns and deletes the nodes for the Trie and sets values such as isWord which is a property denoting if the word ending at this node is a meaningful word from the dictionary, no. of outgoing nodes from each node and the children nodes of each node
 *
 * @author Pallavi
 *
 */
public class TrieNode {
    private boolean isWord;
    private int outNode;
    private TrieNode[] children;

    /**
     * TrieNode constructor, sets default false value for the isWord property, 0 value for the outNode property, and creates an array of child TrieNodes for the TrieNode's children
     */
     public TrieNode() {
        isWord = false;
        outNode = 0;
        children = new TrieNode[26];
    }

    /**
     * function returning if the word ending at the TrieNode forms a word from the dictionary
     * @return a boolean if the word ending at this node is a word in the dictionary
     */
    public boolean isWord() {
        return isWord;
    }

    /**
     * function returning the outgoing nodes from the TrieNode
     * @return int count of the outgoing nodes from this node
     */
    public int getOutNode() {
        return outNode;
    }

    /**
     * function to set the isWord property of the TrieNode
     * @param value the boolean denoting whether the Word object contains a word from the dictionary
     */
    public void setWord(boolean value) {
        isWord = value;
    }

    /**
     * function to return if the node has a child node of the given value
     * @param value if the TrieNode has a childnode with the given value
     * @return value a boolean if the word ending at this node is a word in the dictionary
     */
    public boolean hasChildNode(int value) {
        if(children[value] == null)
            return false;
        else
            return true;
    }

    /**
     * function to add a child node of the given value to the TrieNode and incrementing the outNode count
     * @param value adding a childnode with the given value to the TrieNode
     */
    public void addChildNode(int value) {
        children[value] = new TrieNode();
        outNode++;
    }

    /**
     * function to get a child node of the given value from the TrieNode
     * @param value of the childnode to be returned
     * @return TrieNode with the given value
     */
    public TrieNode getChildNode(int value) {
        return children[value];
    }

    /**
     * function to remove a child node of the given value from the TrieNode and decrementing the outNode count
     * @param value of the childnode to be deleted
     */
    public void removeChildNode(int value) {
        children[value] = null;
        outNode--;
    }
}
