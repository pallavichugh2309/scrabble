package com.pallavi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Scrabble API Test class
 *
 * Test class for handling invalid inputs to the API request
 * @author Pallavi
 *
 */

@ControllerAdvice
public class ControllerExceptionHandler {

    private static Logger log = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR) //To specify the response status of a controller method
    @ExceptionHandler(Exception.class)
    public void notFoundHandler() {
        log.debug("Item not found. HTTP 500 returned.");
    }

}
