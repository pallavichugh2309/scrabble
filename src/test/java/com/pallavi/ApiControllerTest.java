package com.pallavi;

import com.pallavi.AbstractTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Scrabble API Test class
 *
 * This class handles cases where input can be:
 * 1. a String of valid characters and permutations of these characters and subsequences forms a valid word.
 * 2. a String of characters and all permutations of these characters and subsequences form words not found in the dictionary.
 * 3. an invalid String eg. 123 or .hat
 * @author Pallavi
 *
 */

public class ApiControllerTest extends AbstractTest {

    @Override
    @Before //Executed before each test to prepare the test environment (e.g., read input data, initialize the class).
    public void setUp() {
        super.setUp();
    }

    @Test //Test case to check whether a String of valid characters and permutations and subsequences of these characters forms a valid word.
    public void getValidWordsWhenPresent() throws Exception {
        String uri = "/words/hat"; //Test input
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri) //MvcResult interface provides access to the response of an executed request
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus(); //response status when request made from the mentioned endpoint
        assertEquals(200, status); //comparing response status with the expected response status
        String content = mvcResult.getResponse().getContentAsString(); //Getting the JSON response
        String[] words = super.mapFromJson(content, String[].class); //Converting JSON response to an array of Strings
        Set<String> responseWordSet = new HashSet<String>(Arrays.asList(words));
        Set<String> expected = new HashSet<String>(Arrays.asList("hat", "th","ah","ha","at","a"));//Expected outcome for he given test input
        assertEquals(expected, responseWordSet);

    }

    @Test //Test case to check whether a String of characters and all permutations of these characters and subsequences form words not found in the dictionary.
    public void getEmptyWordsWhenNotPresent() throws Exception {
        String uri = "/words/zzz"; //Test input
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)//MvcResult interface provides access to the response of an executed requ
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus(); //response status when request made from the mentioned endpoint
        assertEquals(200, status); //comparing response status with the expected response status
        String content = mvcResult.getResponse().getContentAsString(); //Getting the JSON response
        String[] words = super.mapFromJson(content, String[].class); //Converting JSON response to an array of Strings
        Set<String> responseWordSet = new HashSet<String>(Arrays.asList(words));
        Set<String> expected = new HashSet<String>(); //Expected outcome for he given test input
        assertEquals(responseWordSet, expected);

    }

    @Test //Test case to check an invalid String eg. 123 or .hat
    public void catchErrorWhenInvalidInput() throws Exception {
        String uri = "/words/.hat"; //Test input
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)//MvcResult interface provides access to the response of an executed requ
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus(); //response status when request made from the mentioned endpoint
        assertEquals(500, status); //comparing response status with the expected response status of 500 as Exception is catched for this invalid response

    }
}
